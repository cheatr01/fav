import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Button} from "primereact/button";
import {InputText} from "primereact/inputtext";
import {Dialog} from "primereact/dialog";
import {Panel, PanelHeaderTemplateOptions} from "primereact/panel";

const EditClaims = () => {
    const {userId} = useParams();
    const [claimValues, setClaimValues] = useState({} as { [key: string]: any; });
    const [displayDialog, setDisplayDialog] = useState(false);
    const [newKey, setNewKey] = useState("");

    const fetchUser = () => {
        fetch('/user/' + userId)
            .then((response) => response.json())
            .then((data) => {
                const claims = data.customClaims;
                claims ? setClaimValues(claims) : setClaimValues({});
            });
    };

    const updateClaims = () => {
        const json = JSON.stringify(claimValues);
        console.log(json);
        fetch(`/user/${userId}/claim`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: json,
        }).then();
    }

    useEffect(() => {
        console.log(userId);
        if (userId != undefined) {
            fetchUser();
        } else {
            console.log("userId is undefined");
        }
    }, []);

    const headerTemplate = (options: PanelHeaderTemplateOptions) => {
        const className = `${options.className} justify-content-between gap-8`;
        const titleClassName = `${options.titleClassName} ml-2 text-primary`;
        const style = {fontSize: '1.5rem'};

        return (
            <div className={className}>
                <span className={titleClassName} style={style}>Edit Claims</span>
                <Button rounded icon={"pi pi-plus"}
                        onClick={() => setDisplayDialog(true)}/>
            </div>
        );
    };


    const handleClaimValueChange = (claimKey: string, value: any) => {
        setClaimValues((prevValues) => ({
            ...prevValues,
            [claimKey]: value,
        }));
    };

    const handleAddNewKey = () => {
        setClaimValues((prevValues) => ({
            ...prevValues,
            [newKey]: "",
        }));
        setDisplayDialog(false)
        setNewKey("");
    };

    const handleDeleteClaim = (claimKey: string) => {
        setClaimValues((prevValues) => {
            const newValues = {...prevValues};
            delete newValues[claimKey];
            return newValues;
        });
    }

    const hasClaims = () => {
        return Object.keys(claimValues).length > 0;
    }

    return (
        <div className="flex justify-content-center mt-5">
            <Panel headerTemplate={headerTemplate} className="min-w-max">
                <div className="flex flex-column justify-content-around">
                    {hasClaims() ? Object.entries(claimValues).map(([claimKey, claimLabel]) => (
                        <div className="flex flex-row">
                            <InputText
                                key={claimKey}
                                value={claimKey || ''}
                                disabled={true}
                            />
                            <InputText
                                key={`claimValue for ${claimKey}`}
                                type="text"
                                placeholder={claimLabel}
                                value={claimValues[claimKey] || ''}
                                onChange={(e) => handleClaimValueChange(claimKey, e.target.value)}
                            />
                            <Button icon={"pi pi-times"} rounded severity="danger" key={`delete ${claimKey}`}
                                    size="small" style={{fontSize: '0.75rem'}}
                                    onClick={() => handleDeleteClaim(claimKey)}/>
                        </div>
                    )) : "No claims"}
                    <div className="flex flex-row justify-content-center mt-4">
                        <Button className="max-w-6rem" label="Save" severity="success" icon={"pi pi-save"}
                                onClick={updateClaims} visible={hasClaims()}/>
                    </div>

                    <Dialog onHide={() => setDisplayDialog(false)} visible={displayDialog}>
                        <InputText value={newKey} onChange={(e) => {
                            setNewKey(e.target.value);
                        }}/>
                        <Button onClick={handleAddNewKey}>Add</Button>
                    </Dialog>
                </div>
            </Panel>
        </div>
    );
}

export default EditClaims;