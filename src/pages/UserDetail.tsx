import {useNavigate, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {UserRecord} from "../types";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {Panel} from "primereact/panel";
import {Button} from "primereact/button";

const UserDetail = () => {
    const navigate = useNavigate();

    const {userId} = useParams();
    const [user, setUser] = useState({} as UserRecord);

    const fetchUser = () => {
        fetch('/user/' + userId)
            .then((response) => response.json())
            .then((data) => {
                setUser(data as UserRecord);
            });
    };

    useEffect(() => {
        console.log(userId);
        if (userId != undefined) {
            fetchUser();
        } else {
            console.log("userId is undefined");
        }
    }, []);

    const tableHeader = (
        <div className="flex flex-wrap align-items-center justify-content-between gap-2">
            <span className="text-lg text-900 font-bold">Custom claims</span>
            <Button icon="pi pi-pencil" rounded raised
                    onClick={() => navigate(`/edit/claims/${userId}`)}/>
        </div>
    );

    const columnTemplate = (rowData: { [key: string]: any; }) => {
        return (
            <div>
                {rowData[1]?.toString()}
            </div>
        );
    }

    return (
        <div>
            <Panel header="User Details">
                <div className="flex flex-column text-xl">
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"FirebaseId"}</div>
                        <div className="col-10">{user.uid}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Email"}</div>
                        <div className="col-10">{user.email}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"DisplayName"}</div>
                        <div className="col-10">{user.displayName}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Email verified"}</div>
                        <div className="col-10">{user.emailVerified ? "true" : "false"}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Disabled"}</div>
                        <div className="col-10">{user.disabled ? "true" : "false"}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Created at"}</div>
                        <div className="col-10">{user.metadata?.creationTime}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Last logged"}</div>
                        <div className="col-10">{user.metadata?.lastSignInTime}</div>
                    </div>
                    <div
                        className="flex align-items-center justify-content-center border-bottom-1 border-gray-300">
                        <div className="col-2">{"Last refresh"}</div>
                        <div className="col-10">{user.metadata?.lastRefreshTime}</div>
                    </div>
                </div>
            </Panel>
            {
                <DataTable value={user.customClaims ? Object.entries(user.customClaims as { [key: string]: any; }) : [{}]} header={tableHeader}>
                    <Column field="0" header="Key"/>
                    <Column field="1" header="Value" body={columnTemplate}/>
                </DataTable>
            }
        </div>
    );
}

export default UserDetail;