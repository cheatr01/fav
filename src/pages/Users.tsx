import {useEffect, useState} from "react";
import {DataTable, DataTableSelectionChangeEvent} from "primereact/datatable";
import {Column} from "primereact/column";
import {UserRecord, UsersResponse} from "../types";
import {Button} from "primereact/button";
import {Dialog} from "primereact/dialog";
import {useNavigate} from "react-router-dom";

const Users = () => {
    const navigate = useNavigate();

    const [users, setUsers] = useState([] as UserRecord[]);
    const [visible, setVisible] = useState(false);
    const [selectedUser, setSelectedUser] = useState({} as UserRecord);

    let nextPageToken: string | undefined | null = null;

    const onSelectUser = (e: DataTableSelectionChangeEvent<any>) => {
        navigate(`/user/${(e.value as UserRecord).uid}`)
    }

    const hasUsers = () => {
        return users.length > 0;
    }

    useEffect(() => {
            if (!hasUsers()) {
                fetch(`/users`,)
                    .then(response => response.json() as Promise<UsersResponse>)
                    .then(data => {
                        setUsers(data.users);
                        nextPageToken = data.nextPageToken;
                    });
            }
        }
    )

    const actionTemplate = (rowData: UserRecord) => {
        return (
            <div>
                <Button className="m-1" icon="pi pi-list" rounded tooltip="Show custom claims"
                        tooltipOptions={{position: "left"}}
                        onClick={() => {
                            setSelectedUser(rowData);
                            setVisible(true);
                        }}/>
                <Button className="m-1" icon="pi pi-pencil" rounded tooltip="Edit custom claims"
                        tooltipOptions={{position: "left"}}
                        onClick={() => navigate(`/edit/claims/${rowData.uid}`)}/>
            </div>
        );
    }

    const footer = () => {
        return (
            <div className="flex justify-content-center">
                <Button label="Fetch more" onClick={() => handleFetchMore()}/>
            </div>
        )
    }

    const handleFetchMore = () => {
        fetch(`/users?pageToken=${nextPageToken}`,)
            .then(response => response.json() as Promise<UsersResponse>)
            .then(data => {
                setUsers([...users, ...data.users]);
                nextPageToken = data.nextPageToken;
            });
    }

    return (
        <div>
            <DataTable value={users} selectionMode="single" selection={selectedUser}
                       onSelectionChange={onSelectUser} footer={footer} paginator rows={20}>
                <Column field="uid" header="FirebaseId"/>
                <Column field="email" header="Email"/>
                <Column field="displayName" header="Name"/>
                <Column field="emailVerified" header="Email Verified"/>
                <Column field="disabled" header="Disabled"/>
                <Column body={actionTemplate} header="Actions"/>
            </DataTable>

            <Dialog onHide={() => {
                setVisible(false);
                setSelectedUser({} as UserRecord)
            }}
                    visible={visible}
                    header="Custom Claims"
                    modal
                    keepInViewport={true}
                    style={{width: '80%'}}
            >
                <div>

                    <DataTable
                        value={Object.entries(selectedUser.customClaims ?? {})}
                        columnResizeMode={"fit"}
                        scrollable={true}
                    >
                        <Column field="0" header="Claim"/>
                        <Column field="1" header="Value" resizeable={true}
                                style={{whiteSpace: 'pre-line'}}/>
                    </DataTable>


                </div>
            </Dialog>
        </div>
    );
}

export default Users;