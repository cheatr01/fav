export interface UserRecord {
    uid: string;
    displayName: string;
    email: string;
    emailVerified: boolean;
    disabled: boolean;
    metadata: UserMetadata;
    tenantId: string | null;
    customClaims: { [key: string]: any; };
}

export interface UserMetadata {
    creationTime?: string;
    lastSignInTime?: string;
    lastRefreshTime?: string;
}

export interface UsersResponse {
    users: UserRecord[];
    nextPageToken?: string;
}