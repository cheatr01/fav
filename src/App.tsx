import React from 'react';
import './App.css';
import Header from "./components/Header";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import 'primeicons/primeicons.css';
import '/node_modules/primeflex/primeflex.css';

function App() {
    return (
        <div className="App">
            <div>
                <Header/>
            </div>
        </div>
    );
}

export default App;
