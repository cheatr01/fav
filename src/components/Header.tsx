import {Toolbar} from "primereact/toolbar";
import {Button} from "primereact/button";
import {useNavigate} from "react-router-dom";

const Header = () => {
    const navigate = useNavigate();

    const startTemplate = () => {
        return (
            <h1 onClick={() => navigate("/")}>Firebase View</h1>
        )
    }

    const endTemplate = () => {
        return (
            <div>
                <Button
                    icon="pi pi-search"
                    rounded
                    onClick={() => navigate("/search")}
                />
            </div>
        );
    }

    return (
        <Toolbar
            pt={{
                root: {style: {padding: '0em 2em 0em 2em'}},
            }}
            className="pt-0" start={startTemplate} end={endTemplate}/>
    );
}

export default Header;