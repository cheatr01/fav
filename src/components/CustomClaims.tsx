interface CustomClaimsProps {
    customClaims: object | undefined | null;
}

const CustomClaims = (props: CustomClaimsProps) => {

    return (
            <div>
                {
                    Object.entries(props.customClaims ?? {}).map(([key, value]) => {
                        return (
                            <div className="grid">
                                {/*<div className=" text-xl">*/}
                                    <div className="col-6">{key}</div>  <div className="col-6">{value.toString()}</div>
                                {/*</div>*/}
                            </div>
                        )
                    })
                }
            </div>
    )
}

export default CustomClaims;